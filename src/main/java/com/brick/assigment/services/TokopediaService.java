package com.brick.assigment.services;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service
public class TokopediaService {
	private Logger logger = LogManager.getLogger(getClass().getName());
	@Value("${file.path}")
	private String filePath;
	@Value("${key.scraping}")
	private String keyScraping;

	public JSONObject getData() {
		JSONObject res = new JSONObject();

		List<JSONObject>listResult = new ArrayList<JSONObject>();
		boolean reload = true;
		int page = 1 ;
		int jumlahData = 0;
		JSONArray resArr = new JSONArray();
		
		CSVWriter writer = null;;
		int countColumn = 0;
		try {
			writer = new CSVWriter(new FileWriter(filePath));
			String header = "ID,Name,Price,Image,Rating,MerchantName,Location,Link";
			String line1[] = header.split(",");
			writer.writeNext(line1);
			countColumn = line1.length + 1;
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(reload) {
			String url = "https://app.scrapingbee.com/api/v1?api_key="+keyScraping+"&json_response=true"
					+ "&block_resources=true&url=https://www.tokopedia.com/p/handphone-tablet/handphone?page=" + page; 
			System.err.println(url);
			try {
				
		    	HttpResponse<String> s = Unirest.get(url)
		                .asString();
			    
		        if (Optional.ofNullable(s).isPresent()) {
		        	org.json.JSONObject jsonObject = new org.json.JSONObject(s.getBody());

		        	String body = jsonObject.getString("body").replace("\n", "").replace("\r", "").replace("\\", "");
		        	

		        	String splitData[] = body.split(":\\{\"data\":\\[");

		        	String rawData = "";
		        	for(int a = 0 ; a < splitData.length ; a++) {
		        		rawData = splitData[a];
		        	}
		        	
		        	String []splitRawData = rawData.split("\"\\}\\]");

		        	splitRawData[0] = "[" + splitRawData[0] + "\"}]";
		        	String finalStr = splitRawData[0];

		        	org.json.JSONArray arrRaw = new org.json.JSONArray(finalStr);
		        	
		        	for (int i = 0; i < arrRaw.length(); i++) {
		        		org.json.JSONObject explrObject = arrRaw.getJSONObject(i);
		        		
		        		JSONObject sub = new JSONObject();
		        		JSONObject resData = new JSONObject();
		        		JSONObject resDataProduct = new JSONObject();
		        		JSONObject resDataShop = new JSONObject();
		        		JSONObject resDataImage = new JSONObject();
		        		
		        		String data = "{" + subString(explrObject.get("id") + "\":\\{", ":\"Data\"\\}", body) + ":\"Data\"}";
		        		
		        		org.json.JSONObject dataObj = new org.json.JSONObject(data);
		        		String dataProduct = "{" + subString(dataObj.getJSONObject("product").get("id") + "\":\\{", ":\"TopAdsProduct\"\\}", body) + ":\"TopAdsProduct\"}";
		        		String dataShop = "{" + subString(dataObj.getJSONObject("shop").get("id") + "\":\\{", ":\"TopAdsShop\"\\}", body) + ":\"TopAdsShop\"}";
		        		
		        		org.json.JSONObject dataProductObj = new org.json.JSONObject(dataProduct);
		        		org.json.JSONObject dataShopObj = new org.json.JSONObject(dataShop);
		        		
		        		String dataImage = "{" + subString( "\"\\" + dataProductObj.getJSONObject("image").get("id") + "\":\\{", ":\"Image\"\\}", body) + ":\"Image\"}";
		        		
		        		org.json.JSONObject dataImageObj = new org.json.JSONObject(dataImage);
		        		
		        		resDataProduct.put("id", dataObj.getJSONObject("product").get("id"));
		        		resDataProduct.put("nama", dataProductObj.get("name"));
		        		resDataProduct.put("link", objectToString(dataProductObj.get("uri")).replace("u002F", "/"));
		        		resDataProduct.put("harga", dataProductObj.get("price_format"));
		        		resDataProduct.put("jumlah_review", dataProductObj.get("count_review_format"));
		        		
		        		resDataProduct.put("detail_id", dataProductObj.get("id"));
		        		resDataShop.put("id", dataObj.getJSONObject("shop").get("id"));
		        		resDataShop.put("shop_id", dataShopObj.get("id"));
		        		resDataShop.put("nama", dataShopObj.get("name"));
		        		resDataShop.put("lokasi", dataShopObj.get("location"));
		        		
		        		String imageUrl = objectToString(dataImageObj.get("s_ecs")).replace("u002F", "/");
		        		resDataImage.put("id", dataProductObj.getJSONObject("image").get("id"));
		        		resDataProduct.put("image_url", imageUrl);
		        		
		        		resData.put("product", resDataProduct);
		        		resData.put("shop", resDataShop);
		        		resData.put("image", resDataImage);
		        		
		        		sub.put("data", resData);
		        		sub.put("id", explrObject.get("id"));
		        		
		        		//CSV	ID,Name,Price,Image,Rating,MerchantName,Location,Link
		        		String dataExport[] = new String[countColumn];
		        		dataExport[0] = objectToString(dataProductObj.get("id"));
		        		dataExport[1] = objectToString(dataProductObj.get("name"));
		        		dataExport[2] = objectToString(dataProductObj.get("price_format"));
		        		dataExport[3] = objectToString(imageUrl);
		        		dataExport[4] = objectToString(dataProductObj.get("count_review_format"));
		        		dataExport[5] = objectToString(dataShopObj.get("name"));
		        		dataExport[6] = objectToString(dataShopObj.get("location"));
		        		dataExport[7] = objectToString(dataProductObj.get("uri")).replace("u002F", "/");
		        		
						writer.writeNext(dataExport);
		        		
		        		resArr.add(sub);	 
		        		listResult.add(sub);
		        	}
		        	page++;		        		
		        	
		        	jumlahData += arrRaw.length();
		        	System.err.println("page : " + page);
		        	System.err.println("Jumlah Data" + jumlahData);
		        	if(jumlahData > 50) {
		        		reload = false;
		        	}
		        }
		       
			}catch (Exception e) {
				logger.error(e.getMessage());
			}
			
		}
		
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		res.put("result", resArr);
		res.put("jumlah-data", jumlahData);
		res.put("link-csv", "http://ulilworld.com/download/Tokopedia.csv");
		
		logger.info("DONE");
		return res;
	}
	
	//ensure only result 1 of index array
	private String subString(String start, String end, String master) {
		String []tmpRes ;
		String []tmp = master.split(start);
		String res = "";
		try {			
			tmpRes = tmp[1].split(end);
			res = tmpRes[0];
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
				
		return res;
	}
	
	private String objectToString(Object object) {
		String result;
		try{
			result = object.toString();
		}catch (Exception e) {
			result = "";
		}
		if(object == null) {
			result = "";
		}
		return result;
	}
	
}






