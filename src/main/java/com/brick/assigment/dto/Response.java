package com.brick.assigment.dto;
	
import java.io.Serializable;
import org.springframework.stereotype.Component;

@Component
public class Response implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Object data = null;
	String rd = "";
	String rc = "";
	long total = 1;
	int limit = 1;
	int page = 0;
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getRd() {
		return rd;
	}
	public void setRd(String rd) {
		this.rd = rd;
	}
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
}

