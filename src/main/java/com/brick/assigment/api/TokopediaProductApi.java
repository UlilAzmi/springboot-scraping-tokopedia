package com.brick.assigment.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.brick.assigment.dto.Response;
import com.brick.assigment.services.TokopediaService;


@RestController
@RequestMapping("/tokopedia")
public class TokopediaProductApi {
	
	@Autowired
	private TokopediaService tokpedService;
	
	
	@GetMapping("")
	public ResponseEntity<Response> findAll() {
		Response response = new Response();
		response.setRd("Berhasil membaca saldo");
		response.setRc("00");
		response.setData(tokpedService.getData());
		return ResponseEntity.ok(response);
	}
	
}








